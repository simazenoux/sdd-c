Procédure ABR_profIter_pref2(E : a0):
    m(cour) := cm(a0); 
    m(pile) := initPile(taille); 
    m(H_Max) := 0; 
    m(H_cour) := 1;
    m(nbNoeuds) := 0; 
    m(nbFeuilles) := 0; 
    m(nbPoints) := 0;
    Tant que cm(cour) ≠ NIL faire
        Tant que cm(cm(cour)+1) ≠ NIL faire [ arrêt qdle prochain est une feuille]
            empiler(cm(pile), cm(cour));
            m(nbNoeuds) := cm(nbNoeuds) + 1; [ sur un noeud]
            m(H_cour) := cm(H_cour) + 1;
            m(cour) := cm(cm(cour)+1); [ parcours le lv ]
        fait;
        m(nbFeuilles) := cm(nbFeuilles) + 1; [ sur une feuille]
        Si cm(H_cour) > cm(H_Max) alors
            m(H_Max) := cm(H_cour); 
        fsi;
        m(cour) := cm(cm(cour)+2);
        Tant que cm(cour)=NIL et NON estVide(cm(pile)) alors
            m(cour) := dépiler(cm(pile));
            m(H_cour) := cm(H_cour) –1;
            m(cour) := cm(cm(cour)+2);
        fait;
    fait; 
    m(nbPoints) := cm(nbNoeuds) + cm(nbFeuilles); 
    Libérer(cm(pile));
Fin;