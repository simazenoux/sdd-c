#include "donnees.h"


// d1 < d2
bool inferieur(Donnees_t d1, Donnees_t d2)
{
    return (d1.v<d2.v);
}


// d1 == d2
bool egal(Donnees_t d1, Donnees_t d2)
{
    return (d1.v==d2.v);
}

// d1 > d2
bool superieur(Donnees_t d1, Donnees_t d2)
{
    return (d1.v>d2.v);
}

// Débuggage
void afficherDonnees(Donnees_t d)
{
    printf("m[%d][%d] : %d", d.c.i, d.c.j, d.v);
}


// Permet d'impo
Donnees_t importerDonnees(char * ligne)
{
    Donnees_t d;
    sscanf(ligne, "%d %d %d", &d.v, &d.c.i, &d.c.j);
    return d;
}

char * exporterDonnees(char * dest, Donnees_t d)
{
    sprintf(dest, "%d %d %d", d.v, d.c.i, d.c.j);
    return dest;
}