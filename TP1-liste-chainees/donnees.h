/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                       donnees.h                         */
/*                                                         */
/*           Permet de rendre flexible la structure        */
/*            de donnée d'un tableau d'une matrice         */
/*                                                         */
/*   /!\: Bien modifier les fonctions si la structure est  */
/*                        modifiée                         */
/***********************************************************/


#ifndef DONNEES_TABLEAU_H
#define DONNEES_TABLEAU_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "coordonnees.h"

/*---------------------------------------------------------*/
/*                  STRUCTURES DE DONNEES                  */
/*---------------------------------------------------------*/

typedef struct Donnees{
    int v;
    Coordonnees_t c;
} Donnees_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

bool      inferieur(Donnees_t d1, Donnees_t d2);
bool      egal(Donnees_t d1, Donnees_t d2);
bool      superieur(Donnees_t d1, Donnees_t d2);
void      afficherDonnees(Donnees_t d);
Donnees_t importerDonnees(char * ligne);
char *    exporterDonnees(char * ligne, Donnees_t d);

#endif