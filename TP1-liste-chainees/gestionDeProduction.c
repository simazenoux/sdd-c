#include "gestionDeProduction.h"

/*-------------------------------------------------------------------*/
/*                           kPlusPetitesValeurs                     */
/*                                                                   */
/* Description :    Retourne l'adresse du premier maillon d'une liste*/
/*                  chaînée contenant les k plus petites valeurs     */
/*                  d'une matrice                                    */
/* Entrée :         L'adressse de la structure matrice, k            */
/*                  le nombre de valeurs de la liste (si k négatif,  */
/*                  alors toutes les valeurs sont triés)             */
/* Sortie :         L'adresse maillon précédent                      */
/* Pré-condition :  La matrice est correctement initialisée          */
/*                  k!=0                                             */
/* Post-condition : L'adresse retourné ne pointe pas vers (nil)      */
/*-------------------------------------------------------------------*/

Maillon_t * kPlusPetitesValeurs(Matrice_t * matrice, int k)
{
    Maillon_t * tete = NULL;
    Donnees_t donnees;
    Coordonnees_t c;
    int taille = 0;
    bool insertionReussie = true;
    Maillon_t * nouv;

    c.i = 0;
    c.j = 0;

    while (c.i < matrice->m && insertionReussie)
    {
        c.j = 0;
        while (c.j < matrice->n && insertionReussie)
        {
            
            donnees.c = c;
            donnees.v = matrice->donnees[c.i][c.j];

            // Si la taille de la liste est différente de la taille maximale permise
            // (On privilégie cette solution plutot que < car cela permet
            //  de passer en paramètre une valeur négative pour k
            //  si l'on ne souhaite pas avoir de limite de taille)
            if (taille != k)
            {
                nouv = insererMaillonLCTriee(&tete, donnees);
                if (nouv)
                {
                    taille++;
                }
                else
                {
                    insertionReussie = false;
                }
            }
            else // Si la liste est "pleine"
            {
                // Si la donnée est inférieur à la donnée 
                if (inferieur(donnees, tete->donnees))
                {
                    nouv = insererMaillonLCTriee(&tete, donnees);

                    // Si le nouveau maillon a bien été alloué et que
                    // la liste est non vide
                    if (nouv && tete)
                    {
                        supprimerMaillonLC(&tete);
                    }
                    else
                    {
                        insertionReussie = false;
                    }
                    
                }
            }
            c.j++;
        }
        c.i++;
    }

    // Si une insertion s'est mal déroulée
    if (!insertionReussie)
    {
        // On libère la tete
        libererLC(tete);
    }

    // On renvoie la tete de liste
    return tete;
}   




/*-------------------------------------------------------------------*/
/*                     supprimerUsine                                */
/*                                                                   */
/* Description :    Permet de supprimer toutes les tarifs d'une      */
/*                  usine de la liste                                */
/* Entrée :         Un double pointeur sur tete, les données dont    */
/*                  on veut maillon précédent                        */
/* Sortie :         /                                                */
/* Pré-condition :  /                                                */
/* Post-condition : /!\ LA LISTE PEUT ETRE VIDE /!\                  */
/*-------------------------------------------------------------------*/

void supprimerUsine (Maillon_t ** tete, int usine)
{
    Maillon_t ** pprec = tete;
    Maillon_t * cour = *tete;

    // Tant que la liste est non vide
    while (cour)
    {
        // On supprime le maillon de tête
        if (cour->donnees.c.i == usine)
        {
            supprimerMaillonLC(pprec);
        }
        else
        {
            pprec = &cour->suivant;
        }
        cour = cour->suivant;
    }
}



/*-------------------------------------------------------------------*/
/*                  rechercheLCTriee             */
/*                                                                   */
/* Description :    Insére un maillon dans une liste chainée triée   */
/*                  de la plus grande vers la plus petite valeur     */
/* Entrée :         Un double pointeur sur tete, les données dont    */
/*                  on veut maillon précédent                        */
/* Sortie :         L'adresse maillon précédent                      */
/* Pré-condition :  La liste est non vide                            */
/*                  Les données de la tete sont inférieurs aux       */
/*                  donnees dont on recherche le maillon précédent   */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    FILE * fichierMatrice;
    FILE * fichierLC;
    Matrice_t * matrice;
    Maillon_t * tete = NULL;
    int codeRetour=0;
    int k;
    int i;

    if (argc >= 3)
    {
        k = atoi(argv[2]);
        if (k!=0)
        {
            fichierMatrice = fopen(argv[1], "r");

            // Si l'ouverture du fichier est réussie
            if (fichierMatrice)
            {
                matrice = importerMatrice(fichierMatrice);

                // Si l'importation de la matrice s'est bien déroulée
                if (matrice)
                {
                    // Affichage de la matrice pour debug
                    // afficherMatrice(matrice);
                    tete = kPlusPetitesValeurs(matrice, k);

                    if (tete)
                    {
                        afficherLC(tete);

                        for (i=3; i<argc; i++)
                        {
                            supprimerUsine(&tete, atoi(argv[i]));
                            afficherLC(tete);
                        }

                        fichierLC = fopen("listeChainee.txt", "w");

                        if (fichierLC)
                        {
                            // On exporte la liste chainée
                            exporterLC(tete, fichierLC);
                            // On ferme le fichier d'export
                            fclose(fichierLC);
                        }
                        else
                        {
                            fprintf(stderr, "Erreur : création / ouverture du fichier de sortie de la liste chainée impossible\n");
                            codeRetour = 6;
                        }
                        
                        libererLC(tete);
                    }
                    else // Erreur affectation tableau
                    {
                        fprintf(stderr, "Erreur d'allocation mémoire lors de la création du tableau\n");
                        codeRetour = 5;
                    }
                    libererMatrice(matrice);
                }
                else // Si il y a eu une erreur d'allocation mémoire 
                    //lors de l'importation de la matrice
                {
                    fprintf(stderr, "Erreur d'allocation mémoire lors d'importation de la matrice\n");
                    codeRetour = 4;
                }
                fclose(fichierMatrice);
            }
            else // Si l'ouverture du fichier contenant la matrice a échouée
            {
                fprintf(stderr, "Erreur : Ouverture du fichier contenant la matrice impossible\n");
                fprintf(stderr, "         Vérifiez bien le nom passé en paramètre\n");
                codeRetour = 3;
            }
        }
        else // k=0
        {
            fprintf(stderr, "Erreur : k doit être non nul\n");
            codeRetour = 2;
        }
    }
    else // Si le nombre de paramètre est inférieur 3
    {
        printf("Erreur : vous devez au moins passer en paramètre le nom du fichier contenant la matrice et k \n");
        codeRetour = 1;
    }

    return codeRetour;
}


int compare(int* i, int* j)
{
    int v = 0;
    if (*i != *j)
    {
        if (*i > *j)
        {
            v = 1;
        }
        else
        {
            v = -1;
        }
    }

    return v;
}