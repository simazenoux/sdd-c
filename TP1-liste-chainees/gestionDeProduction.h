
/***********************************************************/
/*Simon Mazenoux-Axel Pronnier                         2021*/
/*---------------------------------------------------------*/
/*                  GestionDeProduction.h                  */
/*                                                         */
/*           Donne les K valeurs minimales d'un            */
/*                 fichier texte en entrée                 */
/*---------------------------------------------------------*/

#ifndef GESTION_PRODUCTION_H
#define GESTION_PRODUCTION_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "matrice.h"
#include "listeChainee.h"

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

Maillon_t * kPlusPetitesValeurs(Matrice_t * matrice, int k);
void        supprimerUsine (Maillon_t ** tete, int usine);

#endif