#include "listeChainee.h"


/*-------------------------------------------------------------------*/
/*                          afficherLC                               */
/*                                                                   */
/* Description :    Permet l'affichage du contenu d'une liste chainée*/
/*                  sur stdout de manière claire                     */
/* Entrée :         L'adresse du premier maillon                     */
/* Sortie :         Le contenu de la liste sur la sortie standard    */
/*-------------------------------------------------------------------*/

void afficherLC(Maillon_t * tete, void (*display)(void*))
{
    Maillon_t * cour = tete;

    printf("Liste chainée :\n");
    while (cour)
    {   
        printf("%p : \n");
        printf("\t- données : %p\n", cour->data);
        display(cour->data);
        printf("\t- suivant : %p\n", (void *) cour->suivant);
        cour = cour->suivant;
    }
    printf("\n");     
}



/*-------------------------------------------------------------------*/
/*                           comparerLC                              */
/*                                                                   */
/* Description :    Permet de comparer le contenu de deux listes     */
/* Entrée :         Les adresses des deux maillons de tête           */
/* Sortie :         Vrai si les deux listes contiennent autant de    */
/*                  maillons et si tous les maillons sont égaux entre*/
/*                  entre eux (voir egal() dans donnees.c)           */
/*                  Faux sinon                                       */
/*-------------------------------------------------------------------*/

int comparerLC(Maillon_t * tete1, Maillon_t * tete2, int (*compare)(void*, void*))
{
    Maillon_t * cour1 = tete1;
    Maillon_t * cour2 = tete2;


    while (cour1 && cour2 && compare(cour1->data, cour2->data) == 0)
    {
        cour1 = cour1->suivant;
        cour2 = cour2->suivant;
    }

    int v;
    if (cour1 && cour2)
    {
        v = compare(cour1->data, cour2->data);
    }
    else
    {
        if (!cour1)
        {
            v = -1;
        }
        if (!cour2)
        {
            v = 1;
        }
        if (!cour1 && !cour2)
        {
            v = 0;
        }
    }

    return v;
}



/*-------------------------------------------------------------------*/
/*                  rechercheLCTriee                                 */
/*                                                                   */
/* Description :    Insére un maillon dans une liste chainée triée   */
/*                  de la plus grande vers la plus petite valeur     */
/* Entrée :         Un double pointeur sur tete et la donnée         */
/*                  à inserer                                        */
/* Sortie :         Un double pointeur sur le maillon                */
/* Pré-condition :  La liste est non vide                            */
/*                  Les données de la tete sont inférieurs aux       */
/*                  donnees dont on recherche le maillon précédent   */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/

Maillon_t ** rechercheLCTriee(Maillon_t ** tete, void* data, bool (*fonction)(void*, void*))
{
    Maillon_t ** pprec;
    Maillon_t * cour;

    pprec = tete;
    cour = *tete;

    while (cour && fonction(cour->data, data))
    {
        pprec = &cour->suivant;
        cour = cour->suivant;
    }

    return pprec;
}





/*-------------------------------------------------------------------*/
/*                       insererMaillonMilieuLC                      */
/*                                                                   */
/* Description :    Insére un maillon dans une liste chainée         */
/* Entrée :         Un pointeur sur le maillon précédent, ainsi que  */
/*                  les données du nouveau maillon                   */
/* Sortie :         Le nouveau maillon correctement inséré           */
/* Pré-condition :  prec != NULL                                     */
/* Post-condition : Vérifier que le maillon retourné est bien alloué */
/*-------------------------------------------------------------------*/

Maillon_t * insererMaillonLC(Maillon_t ** pprec, void* data)
{
    Maillon_t * nouv = malloc(sizeof(Maillon_t));

    if (nouv)
    {
        nouv->data = data;
        nouv->suivant = *pprec;
        *pprec = nouv;
    }

    return nouv;
}


/*-------------------------------------------------------------------*/
/*                   insererMaillonLCTriee                */
/*                                                                   */
/* Description :    Insére un maillon dans une liste chainée triée   */
/*                  de la plus grande vers la plus petite valeur     */
/* Entrée :         Un double pointeur sur tete, les données du      */
/*                  du nouveau maillon, le nombre d'éléments  de la  */
/*                  liste et k la taille maximale de la liste        */
/*                  (si k négatif : pas de taille maximale)          */
/* Sortie :         Faux si un maillon devait être inséré mais       */
/*                  n'a pas pu être alloué                           */
/*                  Vrai sinon                                       */
/* Pré-condition :  prec != NULL, k != 0                             */
/* Post-condition : Vérifier que la fonction renvoie vrai            */
/*-------------------------------------------------------------------*/

Maillon_t * insererMaillonLCTriee(Maillon_t ** tete, void* data, bool (*fonction)(void*, void*))
{
    Maillon_t ** pprec;
    Maillon_t * nouv;

    // On recherche le pointeur vers le maillon
    pprec = rechercheLCTriee(tete, data, fonction);
    nouv = insererMaillonLC(pprec, data);

    return nouv;
}




/*-------------------------------------------------------------------*/
/*                       supprimerMaillonLC                          */
/*                                                                   */
/* Description :    Supprimer un maillon différent de celui de tête  */
/* Entrée :         Un double pointeur sur le maillon précédent      */
/* Sortie :         /                                                */
/* Pré-condition :  /                                                */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/

void supprimerMaillonLC(Maillon_t ** pprec)
{
    Maillon_t * cour;
    cour = *pprec;
    *pprec = cour->suivant;
    free(cour->data);
    free(cour);
}



/*-------------------------------------------------------------------*/
/*                             libérerLC                             */
/*                                                                   */
/* Description :    Livère l'intégralité des maillons d'une liste    */
/* Entrée :         Un pointeur sur le maillon de tête               */
/* Sortie :         /                                                */
/* Pré-condition :  /                                                */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/


void libererLC(Maillon_t * tete)
{
    // Tant que le maillon de tête ne pointe pas vers NULL
    while (tete)
    {
        // On supprime le maillon de tête
        supprimerMaillonLC(&tete);
    }
}




/*-------------------------------------------------------------------*/
/*                            importerLC                             */
/*                                                                   */
/* Description :    Permet d'importer une LC depuis un fichier       */
/* Entrée :         Un pointeur sur le fichier                       */
/* Sortie :         Un pointeur sur le maillon de tête               */
/* Pré-condition :  Le fichier est ouvert, son contenu est correct   */
/* Post-condition : Vérifier que le maillon tete a bien été alloué   */
/*-------------------------------------------------------------------*/


Maillon_t * importerLC(FILE * fichier, void* (*import)(char*))
{
    Maillon_t * tete = NULL;
    Maillon_t * prec;
    Maillon_t * nouv;
    char ligne[TAILLE_BUFFER];

    while (fgets(ligne, TAILLE_BUFFER, fichier))
    {
        nouv = malloc(sizeof(Maillon_t));

        // Si l'allocation du maillon s'est bien déroulée
        if (nouv)
        {
            nouv->data = import(ligne);
            
            // Si la liste est non vide
            if (tete)
            {
                // Le maillon precedent pointe alors vers le nouveau maillon
                prec->suivant = nouv;
            }
            else // Si la liste est vide
            {
                // Le nouveau maillon devient la tête de la liste
                tete = nouv;
            }
            // Et le nouveau maillon devient le précédent
            prec = nouv;
        }
        else // Si l'allocation s'est mal déroulée
        {
            // Si la liste est non vide
            if (tete)
            {
                // On la libère
                libererLC(tete);
            }
        }
    }
    // On dit que le dernier maillon alloué n'a pas de suivant
    nouv->suivant = NULL;

    // Et on retourne le maillon de tête
    return tete;
}




/*-------------------------------------------------------------------*/
/*                            exporterLC                             */
/*                                                                   */
/* Description :    Permet d'exporter une LC vers un fichier         */
/* Entrée :         Un pointeur sur le fichier, un pointeur sur      */
/*                  le maillon de tête                               */
/* Sortie :         /                                                */
/* Pré-condition :  Le fichier est ouvert, son contenu est correct   */
/* Post-condition : Vérifier que le maillon tete a bien été alloué   */
/*-------------------------------------------------------------------*/

void exporterLC (Maillon_t * tete, FILE * fichier)
{
    Maillon_t * cour = tete;
    char ligne [TAILLE_BUFFER];


    while(cour)
    {
        // On initialise la ligne à vide (équivalent *tete='\0')
        strcpy(ligne,"");
        
        // On récupère la ligne contenant les données du maillon
        exporterDonnees(ligne, cour->donnees);
        if (cour->suivant)
        {
            strcat(ligne, "\n");
        }
            
        
        // On insère dans le fichier notre ligne
        fputs(ligne, fichier);
        
        // Et on avance au prochain maillon
        cour = cour->suivant;
    }
}
