/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                      listeChainee.h                     */
/*                                                         */
/*             Gestion d'une liste chainée                 */
/***********************************************************/


#ifndef LISTE_CHAINEE_H
#define LISTE_CHAINEE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "donnees.h"

#define TAILLE_BUFFER 1024


/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/

typedef struct Maillon
{
    void* data;
    struct Maillon * suivant;
} Maillon_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

void         afficherLC(Maillon_t * liste, void (*display)(void*));
Maillon_t *  insererMaillonLC(Maillon_t ** pprec, void* data);
Maillon_t *  insererMaillonLCTriee(Maillon_t ** tete, void* data, bool (*fonction)(void*, void*));
Maillon_t ** rechercheLCTriee(Maillon_t ** tete, void* data, bool (*fonction)(void*, void*));
int          comparerLC(Maillon_t * tete1, Maillon_t * tete2, int (*compare)(void*, void*));
void         supprimerMaillonLC(Maillon_t ** pprec);
void         libererLC(Maillon_t * tete);
Maillon_t *  importerLC(FILE * fichier, void* (*import)(char*));
void         exporterLC (Maillon_t * tete, FILE * fichier);

#endif