#include "matrice.h"


/*-------------------------------------------------------------------*/
/*                          afficherMatrice                          */
/*                                                                   */
/* Description :    Permet l'affichage d'une matrice sur stdout      */
/* Entrée :         La matrice à afficher                            */
/* Sortie :         La matrice sur la sortie stdout                  */
/* Pré-condition :  La matrice est non nul et est bien initialisée   */
/*-------------------------------------------------------------------*/

void afficherMatrice(Matrice_t * matrice)
{
    int i, j;

    printf("Matrice :\n");
    for (i=0; i<matrice->m; i++)
    {
        for (j=0; j<matrice->n; j++)
        {
            printf("%d ", matrice->donnees[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


/*-------------------------------------------------------------------*/
/*                          importerMatrice                          */
/*                                                                   */
/* Description :    Permet d'importer une matrice à partir           */
/*                  d'un fichier précédement ouvert                  */
/* Entrée :         Le fichier ouvert contenant la matrice           */
/* Sortie :         Pointeur sur la structure Matrice_t initialisée  */
/* Pré-condition :  Le fichier est non null                          */
/* Post-condition : Vérifier que l'adresse pointée est non nulle     */
/*-------------------------------------------------------------------*/


Matrice_t * importerMatrice(FILE * fichier)
{
    Matrice_t * matrice;
    Coordonnees_t coordonnees;
    bool erreurMalloc = false;
    char ligne[TAILLE_LIGNE_MAX];
    char * nombre;

    // On alloue la structure matrice
    matrice = malloc(sizeof(Matrice_t));

    // Si la structure est bien allouée
    if (matrice)
    {
        // On récupère les valeurs de m et n dans la première ligne du fichier
        fgets(ligne, TAILLE_LIGNE_MAX, fichier);
        sscanf(ligne, "%d %d", &matrice->m, &matrice->n);
        
        // On alloue la matrice sous forme de tableau de pointeur de taille m
        matrice->donnees = malloc(matrice->m * sizeof(int*));

        // Si l'affectation du tableau de pointeur est réussie
        if (matrice->donnees)
        {
            coordonnees.i = 0;
            while (coordonnees.i < matrice->m && !erreurMalloc)
            {
                matrice->donnees[coordonnees.i] = malloc(matrice->n*sizeof(int));

                // Si l'affectation est réussie
                if (matrice->donnees[coordonnees.i])
                {
                    fgets(ligne, TAILLE_LIGNE_MAX, fichier);
                    nombre = strtok(ligne, " ");
                    for (coordonnees.j=0; coordonnees.j < matrice->n; coordonnees.j++)
                    {
                        matrice->donnees[coordonnees.i][coordonnees.j] = atoi(nombre);
                        nombre = strtok(NULL, " ");
                    }
                    coordonnees.i++;
                }
                else // Si l'affectation a échoué au bout de la i-ème ligne
                { 
                    // On libère les lignes qui ont été affectés
                    for (--coordonnees.i; coordonnees.i>=0; coordonnees.i--)
                    {
                        free(matrice->donnees[coordonnees.i]);
                    }
                    // Puis on libère le tableau de pointeur 
                    free(matrice->donnees);
                    // Puis enfin la structure matrice
                    free(matrice);

                    // Et on passe le booléen d'erreur à faux
                    erreurMalloc = true;
                }
            }
        }
        else // Si l'affectation du tableau de pointeur a échoué
        {
            free(matrice);
        }
    }

    return matrice;
}


/*-------------------------------------------------------------------*/
/*                           libererMatrice                          */
/*                                                                   */
/* Description :    Libère l'espace alloué d'une matrice             */
/* Entrée :         Un pointeur vers la structure matrice            */
/* Sortie :         /                                                */
/* Pré-condition :  La matrice est bien initialisée                  */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/

void libererMatrice(Matrice_t * matrice)
{
    int i;
    for (i=0; i< matrice->m; i++){
        free(matrice->donnees[i]);
    }
    free(matrice->donnees);
    free(matrice);
}