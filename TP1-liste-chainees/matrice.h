/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                       matrice.h                         */
/*           Donne les K valeurs minimales d'un            */
/*                 fichier texte en entrée                 */
/***********************************************************/


#ifndef MATRICE_H
#define MATRICE_H

#define TAILLE_LIGNE_MAX 1024

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coordonnees.h"

/*---------------------------------------------------------*/
/*                  STRUCTURES DE DONNEES                  */
/*---------------------------------------------------------*/

typedef struct Matrice{
    int ** donnees;  // Matrice en elle-meme
    int    m;        // Nombre de lignes
    int    n;        // Nombre de colonnes
} Matrice_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

void        afficherMatrice(Matrice_t * matrice);
Matrice_t * importerMatrice(FILE * fichier);
void        libererMatrice(Matrice_t * matrice);

#endif