#include "cnp.h"

/*-------------------------------------------------------------------*/
/*                          cnpRecursif                              */
/*                                                                   */
/* Description :    Combinaison de p parmi n récursif                */
/* Entrée :         n, p                                             */
/* Sortie :         C de p parmi n                                   */
/* Post-condition : /                                                */
/*-------------------------------------------------------------------*/
unsigned long long cnpRecursif(int n, int p)
{
    unsigned long long res;
    // Condition de sortie
    if (n==p || p==0)
    {
        res = 1;
    }
    else
    {
        res = cnpRecursif(n-1, p) + cnpRecursif(n-1, p-1);
    }
    return res;
}



/*-------------------------------------------------------------------*/
/*                        cnpDerecursifie                            */
/*                                                                   */
/* Description :    Combinaison de p parmi n dérécursifié            */
/* Entrée :         n, p                                             */
/* Sortie :         C de p parmi n                                   */
/* Post-condition : res != 0 (si res = 0 : prbm alloc mémoire)       */
/*-------------------------------------------------------------------*/

// Version alternative

// unsigned long long cnpDerecursifie(int n, int p)
// {
//     Pile_t p0 = initialiserPile(1024);
//     Element_t element;
//     unsigned long long res = 0;
//     bool fin = false;

//     if (p0.base)
//     {
//         element.p = p;
//         element.n = n;

//         // Tant que la pile est vide
//         while (!fin)
//         {
//             while (element.n != element.p && element.p != 0)
//             {
//                 element.n -=1;
//                 empiler(&p0, element);
//                 element.p -=1;
//             }
//             res++;
//             if (estVidePile(p0))
//             {
//                 fin = true;
//             }
//             else
//             {
//                 depiler(&p0, &element);
//             }
//         }
//         // On libère l'espace mémoire
//         libererPile(p0);
//     }

//     return res;
// }

unsigned long long cnpDerecursifie(int n, int p)
{
    Pile_t p0 = initialiserPile(1024);
    Element_t element;
    unsigned long long res = 0;

    if (p0.base)
    {
        element.p = p;
        element.n = n;

        empiler(&p0, element);

        // Tant que la pile est vide
        while (!estVidePile(p0))
        {
            depiler(&p0, &element);
            

            while (element.n != element.p && element.p != 0)
            {
                element.n -=1;
                empiler(&p0, element);
                element.p -=1;
            }
            res++;
        }
        // On libère l'espace mémoire
        libererPile(p0);
    }

    return res;
}


/*-------------------------------------------------------------------*/
/*                              main                                 */
/*                                                                   */
/* Description :    Affiche la combinaison de p parmi n              */
/* Entrée :         p, n                                             */
/* Sortie :         C de p parmi n                                   */
/*-------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    int code, n, p = 0;
    code = 0;

    if (argc == 3)
    {
        p = atoi(argv[1]);
        n = atoi(argv[2]);
        if (n >= p)
        {
            if (n > 0 && p > 0)
            {
                printf("Algo récursif     : %llu\n", cnpRecursif(n,p));
                printf("Algo dérécursifié : %llu\n", cnpDerecursifie(n, p));    
            }
            else
            {
                fprintf(stderr, "Erreur : p et n doivent être positifs\n");
                code = 3;
            }
        }
        else
        {
            fprintf(stderr, "Erreur : p doit être inférieur ou égal à n\n");
            code = 2;
        }
    }
    else
    {
        fprintf(stderr, "Erreur : nombre de paramètres incorrect\n");
        code = 1;
    }
    
    return code;
}