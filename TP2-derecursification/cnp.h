/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                          cnp.h                          */
/*           Donne les K valeurs minimales d'un            */
/*                 fichier texte en entrée                 */
/***********************************************************/

#ifndef CNP_H
#define CNP_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/



/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

unsigned long long cnpRecursif(int n, int p);
unsigned long long cnpDerecursifie(int n, int p);

#endif