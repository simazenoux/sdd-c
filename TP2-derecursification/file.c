#include "file.h"

/*-------------------------------------------------------------------*/
/*                         initialiserFile                           */
/*                                                                   */
/* Description :    Permet d'initialiser une file                    */
/* Entrée :         La taille de la file                             */
/* Sortie :         La structure file_t initialisée                  */
/* Pré-condition :  Element_t est bien défini                        */
/* Post-condition : p.base != NULL                                   */
/*-------------------------------------------------------------------*/
File_t initialiserFile (int t)
{
    File_t f;
    f.taille=t;
    f.nbElem=0;
    f.base= malloc(t*sizeof(Element_t));
    f.deb= 0;
    f.fin= -1;
    return (f);
}


/*-------------------------------------------------------------------*/
/*                          libererfile                              */
/*                                                                   */
/* Description :    Permet de libérer l'espace mémoire d'une file    */
/* Entrée :         L'adresse de la structure file                   */
/* Sortie :         /                                                */
/*-------------------------------------------------------------------*/
void libererFile (File_t f)
{
    free(f.base);
}


/*-------------------------------------------------------------------*/
/*                            emfiler                                */
/*                                                                   */
/* Description :  Ajoute un élément sur le dessus de la file         */
/* Entrée :       L'adresse de la structure file, l'élément à ajouter*/
/* Sortie :       0 si l'ajout s'est bien effectué, 0 sinon          */
/*-------------------------------------------------------------------*/
int enfiler (File_t * f, Element_t e)
{
    int code = 1;
    if ((*f).nbElem!=(*f).taille)
    {
        (*f).fin++;
        (*f).nbElem++;
        (*f).base[(*f).fin%(*f).taille] = e;
        code = 0;
    }
    return (code);
}


/*-------------------------------------------------------------------*/
/*                            defiler                                */
/*                                                                   */
/* Description :    Retire le premier élément de la file             */
/* Entrée :         L'adresse de la structure file, l'adresse de     */
/*                  l'élément qui va recevoir la valeur extraite     */
/* Sortie :         1 si la file est vide, 0 sinon                   */
/*-------------------------------------------------------------------*/
int defiler (File_t * f, Element_t * adrV)
{
    int code = 1;
    if ((*f).nbElem!=0)
    {
        * adrV = (*f).base[(*f).deb%(*f).taille];
        (*f).nbElem--;
        (*f).deb++;
        code = 0;
    }
    return (code);
}

/*-------------------------------------------------------------------*/
/*                         afficherFile                              */
/*                                                                   */
/* Description :    Permet l'affichage du contenu d'une file         */
/*                  sur stdout de manière claire                     */
/* Entrée :         L'adresse de la structure file                   */
/* Sortie :         Le contenu de la file sur la sortie standard     */
/*-------------------------------------------------------------------*/

void afficherFile (File_t * f)
{
    Element_t e;
    while (!defiler(f,&e))
    {
        printf("%d parmi %d\n", e.p, e.n);
    }
}