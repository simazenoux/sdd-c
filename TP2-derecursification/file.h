/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                         file.h                          */
/*                    Gestion d'une file                   */
/***********************************************************/

#ifndef FILE_H
#define FILE_H

#include <stdlib.h>
#include <stdio.h>

/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/

typedef struct Element
{
    int p;
    int n;
} Element_t;

typedef struct File
{
    Element_t * base;
    int         deb;
    int         fin;
    int         taille;
    int         nbElem;
} File_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

File_t initialiserFile (int t);
void   libererFile (File_t f);
int    enfiler (File_t * f, Element_t e); //retourne 0 si réussit, 1 si File pleine
int    defiler (File_t * f, Element_t * adrV);
void   afficherFile (File_t * f);

#endif