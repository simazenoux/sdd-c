#include "pile.h"

/*-------------------------------------------------------------------*/
/*                         initialiserPile                           */
/*                                                                   */
/* Description :    Permet d'initialiser une pile                    */
/* Entrée :         La taille de la pile                             */
/* Sortie :         La structure Pile_t initialisée                  */
/* Pré-condition :  Element_t est bien défini                        */
/* Post-condition : p.base != NULL                                   */
/*-------------------------------------------------------------------*/
Pile_t initialiserPile (int t)
{
    Pile_t p;
    p.taille=t;
    p.sommet=-1;
    p.base = malloc(t*sizeof(Element_t));
    return (p);
}

/*-------------------------------------------------------------------*/
/*                          libererPile                              */
/*                                                                   */
/* Description :    Permet de libérer l'espace mémoire d'une pile    */
/* Entrée :         L'adresse de la structure pile                   */
/* Sortie :         /                                                */
/*-------------------------------------------------------------------*/
void libererPile (Pile_t p)
{
    free(p.base);
}

/*-------------------------------------------------------------------*/
/*                          estVidePile                              */
/*                                                                   */
/* Description :    Permet de savoir si la pile est vide ou non      */
/* Entrée :         L'adresse de la structure pile                   */
/* Sortie :         1 si la pile est vide, 0 sinon                   */
/*-------------------------------------------------------------------*/
int estVidePile (Pile_t p)
{
    return(p.sommet==-1);
}

/*-------------------------------------------------------------------*/
/*                            empiler                                */
/*                                                                   */
/* Description :  Ajoute un élément sur le dessus de la pile         */
/* Entrée :       L'adresse de la structure pile, l'élément à ajouter*/
/* Sortie :       0 si l'ajout s'est bien effectué, 0 sinon          */
/*-------------------------------------------------------------------*/
int empiler (Pile_t * p, Element_t e) //retourne 0 si réussit, 1 si liste pleine
{
    int code = 1;
    if (!((*p).sommet >= ((*p).taille-1)))
    {
        (*p).sommet ++;
        (*p).base[(*p).sommet] = e;
        code = 0;
    }
    return (code);
}


/*-------------------------------------------------------------------*/
/*                            depiler                                */
/*                                                                   */
/* Description :    Retire l'élément du dessus de la pile            */
/* Entrée :         L'adresse de la structure pile, l'adresse de     */
/*                  l'élément qui va recevoir la valeur extraite     */
/* Sortie :         1 si la pile est vide, 0 sinon                   */
/*-------------------------------------------------------------------*/
int depiler (Pile_t * p, Element_t * adrV)
{
    int code = 1;
    if (!estVidePile(*p))
    {
        *adrV = (*p).base[(*p).sommet];
        (*p).sommet --;
        code = 0;  
    }
    return (code);
}

/*-------------------------------------------------------------------*/
/*                         afficherPile                              */
/*                                                                   */
/* Description :    Permet l'affichage du contenu d'une pile         */
/*                  sur stdout de manière claire                     */
/* Entrée :         L'adresse de la structure pile                   */
/* Sortie :         Le contenu de la pile sur la sortie standard     */
/*-------------------------------------------------------------------*/

void afficherPile (Pile_t * p)
{
    Element_t e;
    while (!depiler(p,&e))
    {
        printf("%d parmi %d\n", e.p, e.n);
    }
}