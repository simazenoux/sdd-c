/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                         pile.h                          */
/*                    Gestion d'une pile                   */
/***********************************************************/

#ifndef PILE_H
#define PILE_H

#include <stdlib.h>
#include <stdio.h>

/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/

typedef struct Element
{
    int p;
    int n;
} Element_t;

typedef struct Pile
{
    Element_t * base;
    int         sommet;
    int         taille;
} Pile_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

Pile_t initialiserPile (int t);
void   libererPile (Pile_t p);
int    estVidePile (Pile_t p);
int    empiler (Pile_t * p, Element_t e);
int    depiler (Pile_t * p, Element_t * adrV);
void   afficherPile (Pile_t * p);

#endif