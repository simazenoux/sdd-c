#include "arbre.h"

// But : allouer et initialiser un maillon
// Entrée : adresse pointeur précédent, élément à insérer
// Sortie : le nouveau maillon initialisé
// Post-condition : return != NULL
static Noeud_t * initialiserNoeud(Noeud_t ** pprec, ElementNoeud_t e)
{
    Noeud_t * nouv = malloc(sizeof(Noeud_t));
    if (nouv)
    {
        nouv->e = e;
        nouv->lv = NULL;
        nouv->lh = *pprec;
        *pprec = nouv;
    }
    return nouv;
}



// But : afficher le contenu du dictionnaire en y ajoutant un préfixe
// Entrée : le premier maillon du dictionnaire, un préfixe qui sera affiché avant chaque mot
// Sortie : mots du dictionnaire sur stdout
static void afficherDictionnaireAvecPrefixe (Noeud_t arbre, char * prefixe)
{
    Noeud_t * cour = &arbre;
    Pile_t pile = initialiserPile(TAILLE_PILE);
    
    while (cour)
    {
        empiler(&pile,cour);
        if (isupper(cour->e.c))
        {
            printf("%s", prefixe);
            afficherMotPile(pile);
        }
        
        cour = cour->lv;
        while (!cour && !estVidePile(pile))
        {
            depiler(&pile,&cour);
            cour=cour->lh;
        }
    }
    libererPile (pile);
}


void afficherDictionnaire (Noeud_t arbre)
{
    afficherDictionnaireAvecPrefixe(arbre, "");
}


// But : permet de construire un arbre à partir d'une notation postfixée
// Entrée : l'arbre que l'on souhaite créer
// Sortie : l'adresse du premier maillon
// Post-condition : vérifier que le contenu de l'adresse retourné est différent de NULL
Noeud_t * construireArbre(char* ligne)
{
    Pile_t pile;
    Noeud_t * arbre = NULL;
    Noeud_t * cour;
    Noeud_t ** pprec = &arbre;
    ElementNoeud_t e;
    bool empilerNoeud = true;
    bool constructionReussie = true;
    int i = 0;

    pile = initialiserPile(TAILLE_PILE);

    // Si la pile est correctement initialisée
    if (pile.base) {
        while (ligne[i] && constructionReussie) {
            switch (ligne[i])
            {
                case '*':
                    pprec = &(cour->lv);
                    break;

                case '(':
                    empilerNoeud = true;
                    break;

                case ')':
                    depiler(&pile, &cour);
                    break;
                
                case '+':
                    depiler(&pile, &cour);
                    pprec = &(cour->lh);
                    empilerNoeud = true;
                    break;
                
                default:
                    e.c = ligne[i];
                    cour = initialiserNoeud(pprec, e);
                    if (cour) // Si l'initialisation s'est bien déroulée
                    {
                        *pprec = cour;
                        if (empilerNoeud) 
                        {
                            empiler(&pile, cour);
                            empilerNoeud = false;
                        }
                    }
                    else
                    {
                        constructionReussie = false;
                        libererArbre(arbre);
                    }
            }
            i++;
        }
        libererPile(pile);
    }
    return arbre;
}


// But : recherche triée d'un noeud contenant un caractère spécifié
// Entrée : adresse du pointeur du premier frère, caractere à rechercher en minuscule
// Sortie : pprec vers noeud trouvé, ou pprec vers noeud nécessitant insertion
static Noeud_t ** rechercheFrere(Noeud_t ** pprec, char c)
{
    while (*pprec &&  c > tolower((*pprec)->e.c))
    {
        pprec = &((*pprec)->lh);
    }
    return pprec;
}


// But : insérer un mot dans un arbre vide
// Entrée : l'adresse du pointeur vers notre noeud dans lequel sera inséré les noeuds
// Sortie : un booléen attestant du bon déroulement de l'insertion
static bool insererMotArbreVide(Noeud_t ** arbre, char * mot)
{
    Noeud_t ** pprec = arbre;
    Noeud_t * nouv;
    ElementNoeud_t e;
    bool insertionReussie = true;
    int i=0;

    while (mot[i] && insertionReussie)
    {
        // On insère tous les caractères à la suite en lien verticaux
        if (mot[i+1]) // Si ce n'est pas le dernier caractère du mot
        {
            e.c = tolower(mot[i]); // Alors on passe en minuscule le caractère à insérer
        }
        else
        {
            e.c = toupper(mot[i]); // Sinon, on le passe en majuscule pour dire que c'est bien la fin du mot
        }

        nouv = initialiserNoeud(pprec, e);

        // Si l'allocation s'est bien déroulée
        if (nouv)
        {
            // pprec se place sur le lien vertical du nouveau maillon
            pprec = &(nouv->lv);
        }
        else
        {
            insertionReussie = false;
            libererArbre(*arbre);
        }
        i++;
    }

    return insertionReussie;
}

// But : insérer un mot dans un arbre non vide
// Entrée : l'adresse du pointeur vers le premier noeud de l'arbre
// Sortie : un booléen attestant du bon déroulement de l'insertion
bool insererMot(Noeud_t ** arbre, char * mot)
{
    Noeud_t ** pprec = arbre;
    bool carAEteTrouve = true;
    bool insertionReussie = true;
    int i = 0;

    while (*pprec && carAEteTrouve)
    {
        pprec = rechercheFrere(pprec, tolower(mot[i]));

        if (*pprec && tolower((*pprec)->e.c) == tolower(mot[i]))
        {
            // Cas où on veut insérer un mot compris dans un mot du dictionnaire actuel
            // (ex : insérer bateau alors que bateaux est déjà inséré)
            if (!mot[i+1])
            {
                (*pprec)->e.c = toupper((*pprec)->e.c);
            }
            pprec = &(*pprec)->lv;
            i++;
        }
        else
        {
            carAEteTrouve = false;
        }
    }

    insertionReussie = insererMotArbreVide(pprec, mot+i);

    return insertionReussie;
}



// But : rechercher dans le dictionnaire des mots commencant par un certain motif
// Entrée : un pointeur vers le premier noeud de l'arbre, le motif à rechercher
// Sortie : un booleen indiquant si un mot à été trouvé, et la liste des mots trouvé sur stdout
bool rechercherMotif(Noeud_t * arbre, char * motif)
{
    Noeud_t ** pprec = &arbre;
    bool motTrouve = true;
    int i=0;

    while (*pprec && motif[i] && motTrouve)
    {
        pprec = rechercheFrere(pprec, motif[i]);
        // Si l'élément à bien été trouvé :
        if (*pprec && tolower((*pprec)->e.c) == tolower(motif[i]))
        {
            // Cas où le motif est un mot du dictionnaire
            // (ex : rechercher "beau", avec dans le dictionnaire beau et beaucoup)
            if (!motif[i+1] && isupper((*pprec)->e.c))
            {
                printf("%s\n",motif);
            }
            pprec = &((*pprec)->lv);
            i++;
        }
        else
        {
            // On sort de la boucle
            motTrouve = false;
        }
    }

    if(*pprec && motTrouve)
    {
        afficherDictionnaireAvecPrefixe(**pprec, motif);
    }
    
    return motTrouve;
}

// But : libérer les noeuds alloués d'un arbre
// Entrée : un pointeur sur l'arbre à libérer
// Sortie : /
void libererArbre(Noeud_t * arbre)
{
    Noeud_t * cour = arbre;
    Noeud_t * temp;
    Pile_t pile = initialiserPile(TAILLE_PILE);
    
    while (cour)
    {
        if (cour->lv)
        {
            empiler(&pile,cour->lv);
        }
        temp = cour;
        cour = cour->lh;
        free(temp);
        
        if (!cour && !estVidePile(pile))
        {
            depiler(&pile,&cour);
        }
    }
    libererPile(pile);
}
