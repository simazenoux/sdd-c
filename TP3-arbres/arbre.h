/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                         arbre.h                          */
/*                   Gestion d'un arbre                    */
/***********************************************************/

#ifndef ARBRE_H
#define ARBRE_H

#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pile.h"

#define TAILLE_PILE 64

/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/

typedef struct ElementNoeud
{
    char c;
} ElementNoeud_t;


typedef struct Noeud
{
    ElementNoeud_t e;
    struct Noeud * lv;
    struct Noeud * lh;
} Noeud_t;


/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

void      afficherDictionnaire (Noeud_t arbre);
Noeud_t * construireArbre(char * chaine);
bool      insererMot(Noeud_t ** arbre, char * mot);
bool      rechercherMotif(Noeud_t * arbre, char * motif);
void      libererArbre(Noeud_t * arbre);

#endif