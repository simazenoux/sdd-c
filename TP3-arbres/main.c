#include "arbre.h"
#include "pile.h"

int main()
{
    Noeud_t * arbre = NULL;
    int retour = 0;
    char input [50];
    bool sortie = false;
    bool allocationReussie = true;

    while (!sortie && allocationReussie)
    {
        printf("  - - - - - - MENU - - - - - -  \n");
        printf("1 - Construire un arbre\n");
        printf("2 - Insérer un mot\n");
        printf("3 - Rechercher un mot\n");
        printf("4 - Afficher le dictionnaire\n");
        printf("5 - Sortir du programme\n");

        printf("Choix : ");
        fgets(input, 50, stdin);
        switch (atoi(input))
        {
        case 1:
            printf("Entrez la notation algébrique : ");
            fgets(input, 50, stdin);
            input[strcspn(input, "\n")] = '\0';
            if (input[0] != '\0') // Si l'entrée est non vide
            {
                libererArbre(arbre);
                arbre = construireArbre(input);
                if (arbre)
                {
                    printf("L'arbre a bien été crée");
                }
                else
                {
                    allocationReussie = false;
                }
            }
            else
            {
                printf("Merci d'entrer une chaine non vide");
            }
            printf("\n\n");
            
            break;

        case 2:
            printf("Entrez le mot à insérer dans l'arbre : ");
            fgets(input, 50, stdin);
            input[strcspn(input, "\n")] = '\0';
            if (input[0] != '\0') // Si l'entrée est non vide
            {
                allocationReussie = insererMot(&arbre, input);
                if (allocationReussie)
                {
                    printf("%s a bien été ajouté", input);
                }
            }
            else
            {
                printf("Merci d'entrer une chaine non vide");
            }
            printf("\n\n");
            break;

        case 3:
            printf("Entrez le motif à rechercher en début de mot : ");
            fgets(input, 50, stdin);
            input[strcspn(input, "\n")] = '\0';
            // Si aucun mot n'a été trouvé durant la recherche
            if (!rechercherMotif(arbre, input))
            {
                printf("Aucun mot n'a été trouvé");
            }
            printf("\n\n");
            break;

        case 4:
            if (arbre)
            {
                printf(" --- ARBRE --- \n");
                afficherDictionnaire(*arbre);
                printf("\n\n");
            }
            else
            {
                fprintf(stderr, "L'arbre est actuellement vide\n\n");
            }
            break;

        case 5:
            sortie = true;
            break;
        
        default:
            fprintf(stderr, "Erreur : Veuillez entrer un chiffre entre 1 et 5\n\n");
            break;
        }
    }

    if (!allocationReussie)
    {
        fprintf(stderr, "Erreur d'allocation mémoire, sortie du programme\n\n");
        retour = 1;
    }
    else
    {
        printf("Au revoir !\n");
    }

    libererArbre(arbre);

    return retour;
}