/***********************************************************/
/* Simon Mazenoux - Axel Pronnier                     2021 */
/*---------------------------------------------------------*/
/*                         pile.h                          */
/*                    Gestion d'une pile                   */
/***********************************************************/

#ifndef PILE_H
#define PILE_H

#include <stdlib.h>
#include <stdio.h>
#include "arbre.h"

/*---------------------------------------------------------*/
/*                  STRUCTURE DE DONNEES                   */
/*---------------------------------------------------------*/

// typedef Noeud_t Noeud_t;

typedef struct Noeud * ElementPile_t;

typedef struct Pile
{
    ElementPile_t * base;
    int         sommet;
    int         taille;
} Pile_t;

/*---------------------------------------------------------*/
/*                         FONCTIONS                       */
/*---------------------------------------------------------*/

Pile_t initialiserPile (int t);
void   libererPile (Pile_t p);
int    estVidePile (Pile_t p);
int    empiler (Pile_t * p, ElementPile_t e);
int    depiler (Pile_t * p, ElementPile_t * adrV);
void   afficherMotPile (Pile_t p);

#endif